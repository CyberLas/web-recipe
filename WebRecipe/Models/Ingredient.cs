﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Ingredient
    {
        public int IdIngredient { get; set; }
        [Required]
        public String Nombre { get; set; }
        [Required]
        public String Cantidad { get; set; }
        [Required]
        public int Costo { get; set; }
        public int IdDish { get; set; }
    }
}
