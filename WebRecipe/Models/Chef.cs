﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Chef
    {
        public int IdChef       { get; set; }
        [Required]
        public String Nombre    { get; set; }
        [Required]
        public String Foto      { get; set; }
        [Required]
        public String Correo { get; set; }
        [Required] 
        public String Password { get; set; }

    }
}
