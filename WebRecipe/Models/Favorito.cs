﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecipe.Models
{
    public class Favorito
    {
        public int IdFavorito   { get; set; }
        public int IdChef       { get; set; }
        public int IdDish       { get; set; }
        public int FavoritoStar { get; set; }
    }
}
