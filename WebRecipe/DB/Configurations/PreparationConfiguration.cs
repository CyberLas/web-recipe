﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRecipe.Models;

namespace WebRecipe.DB.Configurations
{
    public class PreparationConfiguration : IEntityTypeConfiguration<Preparation>
    {
        public void Configure(EntityTypeBuilder<Preparation> builder)
        {
            builder.ToTable("Preparation");
            builder.HasKey(o => o.IdPreparation);
        }
    }
}
