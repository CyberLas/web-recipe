﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRecipe.DB;
using WebRecipe.Models;

namespace WebRecipe.Repositorios
{
    public interface IDashboardRespository
    {
        public List<Follow>   Follow(int id);
        public List<Favorito> Start(int id, int star);
        public List<Chef>     Profile(int id);
        public int            Encontre(String name);
        public int            QuitarFavorito(int id);
        public int            InsertarUsuario(Chef chef);
    }

    public class DashboardRespository : IDashboardRespository
    {
        private readonly AppPruebaContext context;
        public object ViewBag { get; private set; }

        public List<Follow> Follow(int id)
        {
            if (id > 0) {
                return context.Follows.Where(o => o.IdCheff == id).ToList();
            }
            return null;
        }


        public List<Favorito> Start(int id, int star)
        {
            if (id > 0 && star > 0) {
                return context.Favoritos.Where(o => o.IdChef == id).
                    Where(o => o.FavoritoStar == star).ToList();
            }
            return null;
        }

        public List<Chef> Profile(int id)
        {
            if (id > 0)
            {
                return context.Chefs.Where(o => o.IdChef == id).ToList();
            }
            return null;
        }


        public int Encontre(String name)
        {
            if (name != null) {
                if (name == null)
                {
                    var dish = context.Dishes.ToList();
                }
                else
                {
                    var dish = context.Dishes.Where(o => o.Plato.Contains(name));
                }
                var Chef = context.Chefs.ToList();
                var Promedio = context.Favoritos.Where(o => o.FavoritoStar.ToString() == name);
                return 1;
            }
            return 0;
        }


        public int QuitarFavorito(int id)
        {
            if (id > 0) {
                var usuarioid = context.Chefs.Where(o => o.IdChef == id).FirstOrDefault();

                var favorito = context.Favoritos.Where(o => o.IdChef == id)
                    .Where(o => o.IdDish == id).FirstOrDefault();
                context.Favoritos.Remove(favorito);
                context.SaveChanges();
            }
            return 0;
        }

        public int InsertarUsuario(Chef chef)
        {
            var receta = new Chef();
            if (chef.Nombre != null && chef.Password != null && chef.Foto != null && chef.Correo != null) {
                receta.Nombre   = chef.Nombre;
                receta.Foto     = chef.Foto;
                receta.Correo   = chef.Correo;
                receta.Password = chef.Password;
                context.Chefs.Add(receta);
                context.SaveChanges();
                return 1;
            }
            return 0;
        }
    }
}
