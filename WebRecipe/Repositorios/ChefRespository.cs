﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRecipe.DB;
using WebRecipe.Models;

namespace WebRecipe.Repositorios
{
    public interface IChefRepository
    {
        public List<Chef>     Profile  (int id);
        public List<Follow>   Follow   (int id);
        public List<Favorito> Favorite (int id);
    }

    public class ChefRepository : IChefRepository
    {
        private readonly AppPruebaContext context;

        public List<Favorito> Favorite(int id)
        {
            if (id > 0)
            {
                return context.Favoritos.Where(o => o.IdChef == id).ToList();
            }
            return null;
        }

        public List<Follow> Follow(int id)
        {
            if (id > 0) 
            {
                return context.Follows.Where(o => o.IdFollow == id).ToList();
            }
            return null;
        }

        public List<Chef> Profile(int id)
        {
            if (id > 0)
            {
                return context.Chefs.Where(o => o.IdChef == id).ToList();
            }
            return null;
        }
    }
}
