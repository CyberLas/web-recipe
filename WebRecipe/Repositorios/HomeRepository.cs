﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebRecipe.DB;
using WebRecipe.Models;

namespace WebRecipe.Repositorios
{
    public interface IHomeRepository
    {
        public List<Dish> Random6Dish();
        public int Cero();
        public int CreateChef(Chef model);
        public List<Chef> UserLogin(String email, String password);
    }

    public class HomeRepository : IHomeRepository
    {
        private readonly AppPruebaContext context;

        /* .................::::::::::::: Muestar 6 Platos :::::::::::::................. */
        public List<Dish> Random6Dish()
        {
            return context.Dishes.ToList().OrderBy(o => Guid.NewGuid()).Take(6).ToList();
        }


        /* .................::::::::::::: Retorna 0 :::::::::::::................. */
        public int Cero()
        {
            return 0;
        }


        /* .................::::::::::::: CreateChef Formulario :::::::::::::................. */
        public int CreateChef(Chef model)
        {
            if (model.Nombre != null && model.Foto != null && model.Correo != null && model.Password != null) {
                var chef      = new Chef();
                chef.Nombre   = model.Nombre;
                chef.Foto     = model.Foto;
                chef.Correo   = model.Correo;
                chef.Password = model.Password;
                context.Chefs.Add(chef);
                context.SaveChanges();
                return 1;
            }
            return 0; 
        }

        /* .................::::::::::::: Login Email :::::::::::::................. */
        public List<Chef> UserLogin(String email, String password)
        {
            if (email != null  && password != null) {
                return context.Chefs.Where(o => o.Correo == email && o.Password == password).ToList();
            }
            return null;
        }
    }
}
