﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebRecipe.DB;
using WebRecipe.Extensions;
using WebRecipe.Models;
using WebRecipe.Repositorios;

namespace WebRecipe.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly IDashboardRespository app;

        public DashboardController(IDashboardRespository app)
        {
            this.app = app;
        }

        [HttpGet]
        public IActionResult Follow(int id)
        {
            ViewBag.Follow  = app.Follow(id);
            return View();
        }

        [HttpPost]
        public IActionResult Start(int id, int star)
        {
            var favorito          = new Favorito();
            var Favorite          = app.Start(id, star);
            favorito.IdChef       = id;
            favorito.IdDish       = star;
            favorito.FavoritoStar = Favorite.Count();
            return View();
        }

        [HttpGet]
        public IActionResult Comment()
        {
            var context     = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");
            ViewBag.User    = usserLogged;
            ViewBag.Cheff   = app.Profile(usserLogged.IdChef);
            ViewBag.Comment = context.Comments.Where(o => o.IdChef == usserLogged.IdChef).ToList();
            return View();
        }

        [HttpGet]
        public IActionResult Buscar(int id)
        {
            var model = app.Profile(id);
            return View(model);
        }

        [HttpGet]
        public IActionResult Encontre(String name)
        {
            if (app.Encontre(name) != 0) {
                return View();
            }
            return null;
        }

        [HttpGet]
        public IActionResult QuitarFavorito(int id)
        {
            if (app.QuitarFavorito(id) != 0) { 
                return Redirect("/Dashboard/Recipe?id=" + id);
            }

            return Redirect("/Dashboard/Recipe");
        }

        [HttpPost]
        public IActionResult InsertarUsuario(Chef chef)
        {
            var receta = app.InsertarUsuario(chef);
            if (receta != 0)
            {
                return Redirect("/Dashboard/Index");
            }
            return View();
        }

        [HttpGet]
        public IActionResult AñadirFavorito(int id)
        {
            var context     = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");
            var usuarioid   = context.Chefs.Where(o => o.IdChef == usserLogged.IdChef).FirstOrDefault();
            var favorito    = new Favorito();

            return Redirect("/Dashboard/Recipe?id=" + id);
        }

        [HttpGet]
        public IActionResult Favorite()
        {
            var context = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");
            ViewBag.Dish = context.Dishes.ToList();
            ViewBag.Favorite = context.Favoritos.Where(o => o.IdChef == usserLogged.IdChef).ToList();
            return View();
        }

        [HttpGet]
        public IActionResult Profile()
        {
            var context = new AppPruebaContext();
            var usserLogged = HttpContext.Session.Get<Chef>("SessionLoggedUser");
            ViewBag.Cheff = context.Chefs.Where(o => o.IdChef == usserLogged.IdChef).ToList();
            return View();
        }

    }
}
