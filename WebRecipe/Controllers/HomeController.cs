﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Expressions;
using Microsoft.Extensions.Logging;
using WebRecipe.DB;
using WebRecipe.Extensions;
using WebRecipe.Models;
using WebRecipe.Repositorios;

namespace WebRecipe.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeRepository app;

        public HomeController(IHomeRepository app)
        {
            this.app = app;
        }


        /* .................::::::::::::: Muestar 6 Platos :::::::::::::................. */
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Dishes = app.Random6Dish();
            return View();
        }


        /* .................::::::::::::: Retorna La Pagina :::::::::::::................. */
        [HttpGet]
        public IActionResult Login()
        {
            return View(app.Cero());
        }

        /* .................::::::::::::: Retorna La Pagina :::::::::::::................. */
        [HttpGet]
        public IActionResult CreateChef()
        {
            return View(app.Cero());
        }


        [HttpPost]
        public IActionResult CreateChef(Chef model)
        {
            var context = app.CreateChef(model);
            if (context ==0)
            {
                return RedirectToAction("Login", "Home");
            }
            return View(model);
        }


        /* .................::::::::::::: Login User :::::::::::::................. */
        [HttpPost]
        public IActionResult Login(string email, string password)
        {
            var user = app.UserLogin(email, password);

            if (user == null)
                return View();

            HttpContext.Session.Set("SessionLoggedUser", user);

            return RedirectToAction("Index", "Dashboard");
        }


        /* .................::::::::::::: Retorna La Pagina :::::::::::::................. */
        public IActionResult Error()
        {
            return View(app.Cero());
        }
    }
}
