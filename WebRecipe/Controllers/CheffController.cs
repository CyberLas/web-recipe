﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Expressions;
using Microsoft.Extensions.Logging;
using WebRecipe.DB;
using WebRecipe.Extensions;
using WebRecipe.Models;
using WebRecipe.Repositorios;

namespace WebRecipe.Controllers
{
    public class CheffController : Controller
    {
        private readonly IChefRepository app;

        public CheffController(IChefRepository app)
        {
            this.app = app;
        }

        [HttpGet]
        public ActionResult Profile(int id) 
        {
            ViewBag.Cheff = app.Profile(id);
            return View();
        }

        [HttpGet]
        public ActionResult Follow(int id)
        {
            ViewBag.Follow = app.Follow(id);
            return View();
        }

        [HttpGet]
        public ActionResult Favorite(int id)
        {
            ViewBag.Follow = app.Favorite(id);
            return View();
        }
    }
}
