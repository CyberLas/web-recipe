﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WebRecipe.Controllers;
using WebRecipe.Models;
using WebRecipe.Repositorios;

namespace WebRecipeNUnit.ControllerTest
{
    public class HomeControllerTest
    {
        /* .................::::::::::::: Muestar 6 Platos :::::::::::::................. */
        [Test]
        public void SeisPlatosRandom()
        {
            var app        = new Mock<IHomeRepository>();
            app.Setup(o => o.Random6Dish()).Returns(new List<Dish>());

            var controller = new HomeController(app.Object);
            var index      = controller.Index();
            Assert.IsInstanceOf<ViewResult>(index);
        }


        /* .................::::::::::::: Redirecciona a la Pagina Login :::::::::::::................. */
        [Test]
        public void LoginPageRedireccionaraMismaPagina()
        {
            var app        = new Mock<IHomeRepository>();
            var controller = new HomeController(app.Object);

            var index      = controller.Login();
            Assert.IsInstanceOf<IActionResult>(index);
        }


        /* .................::::::::::::: Redirecciona a la Pagina CreateChef :::::::::::::................. */
        [Test]
        public void CreateChefPageRedireccionaraMismaPagina()
        {
            var app        = new Mock<IHomeRepository>();
            var controller = new HomeController(app.Object);

            var index      = controller.CreateChef();
            Assert.IsInstanceOf<IActionResult>(index);
        }


        /* .................::::::::::::: CreateChef Formulario :::::::::::::................. */
        [Test]
        public void CreateChefForm()
        {
            var app = new Mock<IHomeRepository>();
            app.Setup(o => o.Random6Dish()).Returns(new List<Dish>());

            var controller = new HomeController(app.Object);
            var index = controller.CreateChef();
            Assert.IsInstanceOf<ViewResult>(index);
        }


        /* .................::::::::::::: Redirecciona a la Pagina Error :::::::::::::................. */
        [Test]
        public void ErrorPageRedireccionaraMismaPagina()
        {
            var app = new Mock<IHomeRepository>();
            var controller = new HomeController(app.Object);

            var index = controller.Error();
            Assert.IsInstanceOf<IActionResult>(index);
        }

        /* .................::::::::::::: Login User :::::::::::::................. */
        [Test]
        public void LoginUser()
        {
            var app = new Mock<IHomeRepository>();
            app.Setup(o => o.UserLogin("admin@admin.com", "admin")).Returns(new List<Chef>());

            var controller = new HomeController(app.Object);
            var index = controller.Login();
            Assert.IsInstanceOf<ViewResult>(index);
        }

    }
}
