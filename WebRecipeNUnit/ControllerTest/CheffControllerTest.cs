﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WebRecipe.Controllers;
using WebRecipe.Models;
using WebRecipe.Repositorios;

namespace WebRecipeNUnit.ControllerTest
{
    public class CheffControllerTest
    {
        /* .................::::::::::::: Muestra Datos de su Perfil :::::::::::::................. */
        [Test]
        public void VerPerfil()
        {
            var app = new Mock<IChefRepository>();
            app.Setup(o => o.Profile(1)).Returns(new List<Chef>());

            var controller = new CheffController(app.Object);
            var index = controller.Profile(1);
            Assert.IsInstanceOf<ViewResult>(index);
        }


        /* .................::::::::::::: Muestra los Seguidores :::::::::::::................. */
        [Test]
        public void VerFollow()
        {
            var app = new Mock<IChefRepository>();
            app.Setup(o => o.Follow(2)).Returns(new List<Follow>());

            var controller = new CheffController(app.Object);
            var index = controller.Follow(2);
            Assert.IsInstanceOf<ViewResult>(index);
        }


        /* .................::::::::::::: Muestra los Platos Favoritos :::::::::::::................. */
        [Test]
        public void VerFavorito()
        {
            var app = new Mock<IChefRepository>();
            app.Setup(o => o.Favorite(3)).Returns(new List<Favorito>());

            var controller = new CheffController(app.Object);
            var index = controller.Favorite(3);
            Assert.IsInstanceOf<ViewResult>(index);
        }
    }
}
