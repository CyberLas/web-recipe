﻿using Moq;
using System;
using System.Text;
using NUnit.Framework;
using WebRecipe.Models;
using WebRecipe.Controllers;
using WebRecipe.Repositorios;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace WebRecipeNUnit.ControllerTest
{
    public class DasboardControllerTest
    {
        /* .................::::::::::::: Muestra Mostrar Seguidor :::::::::::::................. */
        [Test]
        public void VerPerfil()
        {
            var app        = new Mock<IDashboardRespository>();
            app.Setup(o => o.Follow(1)).Returns(new List<Follow>());

            var controller = new DashboardController(app.Object);
            var index      = controller.Follow(1);
            Assert.IsInstanceOf<ViewResult>(index);
        }


        /* .................::::::::::::: Dar Estrellas a una Receta :::::::::::::................. */
        [Test]
        public void EstrellasdeunaReceta()
        {
            var app         = new Mock<IDashboardRespository>();
            app.Setup(o => o.Start(1, 5)).Returns(new List<Favorito>());

            var controller  = new DashboardController(app.Object);
            var index       = controller.Start(1, 5);
            Assert.IsInstanceOf<IActionResult>(index);
        }


        /* .................::::::::::::: Nostrar Datos del Seguidor :::::::::::::................. */
        [Test]
        public void PerfildelSeguidor()
        {
            var app = new Mock<IDashboardRespository>();
            app.Setup(o => o.Start(1, 5)).Returns(new List<Favorito>());

            var controller = new DashboardController(app.Object);
            var index = controller.Start(1, 5);
            Assert.IsInstanceOf<IActionResult>(index);
        }
        

        /* .................::::::::::::: Insertar usuario Nulo :::::::::::::................. */
        [Test]
        public void InsertarUsuario()
        {
            var app = new Mock<IDashboardRespository>();
            var controller = new DashboardController(app.Object);

            var index = controller.InsertarUsuario(null);
            Assert.IsInstanceOf<IActionResult>(index);
        }


        /* .................::::::::::::: Muestra Mostra Platos con nombre Arroz :::::::::::::................. */
        [Test]
        public void EncontrePlatosConArroz()
        {
            var id = 1;
            var app = new Mock<IDashboardRespository>();
            app.Setup(o => o.Encontre("Arroz")).Returns(id);

            var controller = new DashboardController(app.Object);
            var index = controller.Encontre("Arroz");
            Assert.IsInstanceOf<ViewResult>(index);
        }
    }
}
